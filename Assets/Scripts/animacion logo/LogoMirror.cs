﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogoMirror : MonoBehaviour
{
    public float speed;
    public float limite;
    public float desaceleracion;


    // Update is called once per frame
    void Update()
    {
        transform.Translate(0, speed * Time.deltaTime, 0);

        if (transform.position.y < limite)
        {
            speed += desaceleracion;
        }

        if (speed >= 0)
        {
            speed = 0;
        }
    }
}   