﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogoManagerIV : MonoBehaviour
{
    public float speed;
    public float limite;
    public float desaceleracion;


    // Update is called once per frame
    void Update()
    {	
		transform.Translate(speed * Time.deltaTime, 0, 0);

		if (transform.localPosition.x > limite)
		{
			speed -= desaceleracion;
		}

		if (speed <= 0)
		{
			speed = 0;
		}
    }
}