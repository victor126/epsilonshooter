﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LogoManager : MonoBehaviour
{
    public float timer;
    [SerializeField] AudioSource audioSource;
    // Update is called once per frame
    private void Start()
    {
        audioSource.Play();
    }
    void Update()
    {
        timer += 1f * Time.deltaTime;

        if(Input.anyKeyDown || timer >= 11.0000f)
        {
            SceneManager.LoadScene("Title");
        }
       

        
    }
}
