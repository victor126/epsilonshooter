﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boton1 : MonoBehaviour
{

    [SerializeField] AudioSource audioSource;
    public AudioSource audioSource2;
    public AudioClip select;
    public AudioClip click;

    // Start is called before the first frame update
    void Start()
    {
        audioSource.Play();
    }

    public void selectSound()
    {
        audioSource2.PlayOneShot(select);
    }

    public void clickSound()
    {
        audioSource2.PlayOneShot(click);
    }
}