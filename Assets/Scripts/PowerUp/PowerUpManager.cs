﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpManager : MonoBehaviour
{
    public GameObject GunPowerUp;
    public float timeLauchGunPowerUp;

    private float currentTime = 0;

    void Awake()
    {
        StartCoroutine(LanzaGunPowerUp());
    }

    // Update is called once per frame
    
    void Update()
    {
        currentTime += Time.deltaTime;
        if(currentTime>timeLauchGunPowerUp){
            currentTime = 0;
            Instantiate(GunPowerUp,new Vector3(10,Random.Range(-5,5)),Quaternion.identity,this.transform);
        }
    }
   

    IEnumerator LanzaGunPowerUp()
    {
        while (true)
        {
            Instantiate(GunPowerUp, new Vector3(10, Random.Range(-5, 5)), Quaternion.identity, this.transform);
            yield return new WaitForSeconds(timeLauchGunPowerUp);
        }
    }

}